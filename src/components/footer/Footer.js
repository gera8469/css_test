import React from 'react';
import logoFooter from '../content/svg/logo_footer.svg';

export default function Footer() {
  return (
      <footer className='footer'>
        <div className='content'>
          <div className='footerInfo'>
            <div className='totals'>
              <div className='totals__registered'>
                <p className='totals__value'>2,345,870</p>
                <p className='totals__label'>REGISTERED TASKERS</p>
              </div>
              <div className='totals__posted'>
                <p className='totals__value'>12,675,298</p>
                <p className='totals__label'>TOTAL TASKS POSTED</p>
              </div>
            </div>
            <div className='socials'>
              <i className="fa fa-facebook socials__item" aria-hidden="true">
              </i>
              <i className="fa fa-twitter socials__item" aria-hidden="true">
              </i>
              <i className="fa fa-linkedin socials__item" aria-hidden="true">
              </i>
            </div>
          </div>
          <div className='menu'>
            <div className='menu__block'>
              <p className='menu__title'>Main</p>
              <ul className='menu__submenuItemList'>
                <li className='menu__submenuItem'>Profile</li>
                <li className='menu__submenuItem'>My Tasks</li>
                <li className='menu__submenuItem'>Messages</li>
              </ul>
            </div>
            <div className='menu__block'>
              <p className='menu__title'>About</p>
              <ul className='menu__submenuItemList'>
                <li className='menu__submenuItem'>About Us</li>
                <li className='menu__submenuItem'>How it works</li>
                <li className='menu__submenuItem'>Team</li>
                <li className='menu__submenuItem'>Security</li>
              </ul>
            </div>
            <div className='menu__block'>
              <p className='menu__title'>F.A.Q.</p>
              <ul className='menu__submenuItemList'>
                <li className='menu__submenuItem'>How to Become a Tasker</li>
                <li className='menu__submenuItem'>Ask Question</li>
              </ul>
            </div>
            <div className='menu__block'>
              <p className='menu__title'>Help</p>
              <ul className='menu__submenuItemList'>
                <li className='menu__submenuItem'>Terms and Conditions</li>
                <li className='menu__submenuItem'>Get Support</li>
                <li className='menu__submenuItem'>Report Bug</li>
                <li className='menu__submenuItem'>Contact Us</li>
              </ul>
            </div>
          </div>
          <div className='copyright'>
            <img src={logoFooter} alt="" className='copyright__logo'/>
            <p className='copyright__content'>
              2017 @ Copyright Djob.ch  All rights reserved
            </p>
          </div>
        </div>
      </footer>
  );
}
