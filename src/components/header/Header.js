import React from 'react';
import logo from './images/purse.png';
import '../stylesheet.css';
import userIcon from '../content/svg/userIcon.svg';

export default function Header() {
  return (
      <header className='header'>
        <div className='header__content'>
          <div className='logo flex-v-center'>
            <img
                src={logo}
                alt="" className='logo__image'
            />
            <a href="#" className='logo__link'>DJOB.CH</a>
          </div>
          <div className='profileStatus flex-v-center'>
            <a href='/' className='profileStatus__review profileStatus__item'>
              <i className="material-icons">&#xE0CA;</i>
            </a>
            <a href="/"
               className='profileStatus__notification profileStatus__item'>
              <i className="material-icons">&#xE7F4;</i>
              <span className='profileStatus__counter flex-v-center'>6</span>
            </a>
            <div className
                     ='profileStatus__userMenu flex-v-center
                     profileStatus__item dropdown'>
              <img src={userIcon}
                   alt="" className='profileStatus__userMenu__userIcon'/>
              <span>Alice Cooper</span>
              <i className="fa fa-caret-down profileStatus__caret"
                 aria-hidden="true">
              </i>
              <div className="dropdown__content">
                <p>
                  <a href='/'>Some link</a>
                </p>
                <p>
                  <a href='/'>Another link</a>
                </p>
              </div>
            </div>
            <div className='profileStatus__lang profileStatus__item dropdown'>
              <span>en</span>
              <i className="fa fa-caret-down profileStatus__caret"
                 aria-hidden="true">
              </i>
              <div className="dropdown__content">
                <p>
                  <a href='/'>ru</a>
                </p>
                <p>
                  <a href='/'>ua</a>
                </p>
              </div>
            </div>
          </div>

        </div>
      </header>
  );
}
