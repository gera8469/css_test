import React from 'react';
import portfolio1 from './images/portfolio_1.png';
import portfolio2 from './images/portfolio_2.png';
import portfolio3 from './images/portfolio_3.png';
import defaultAvatar from './images/default_avatar.png';
import avatarDb from './images/avatar_db.png';
import avatarJm from './images/avatar_jm.png';
import imageDummy from './svg/imageDummy.svg';
import imageIcon from './svg/image_icon.svg';

export default function Content() {
  return (
      <div className='wrapper'>
        <div className='content'>
          <div className='profileHeader flex-v-center'>
            <div className='profileHeader__info'>
              <img src={defaultAvatar} alt="" className='profileHeader_avatar'
              />
              <div className='profileHeader__info__textInfo'>
                <h1 className='profileHeader__name'>Alice Cooper</h1>
                <p className='profileHeader__location flex-v-center'>
                  <i className="material-icons">&#xE55F;</i>
                  Berlin, Germany
                </p>
              </div>
            </div>
            <button className={'profileHeader__button'}>
              INVITE TO PERFORM
            </button>
          </div>
          <div className='profileContent'>
            <section className='about'>
              <div className='blockTitle flex-v-center'>
                <i className
                       ="material-icons blockTitle__icon blockTitle__icon_round"
                >&#xE236;</i>
                <h2 className='blockTitle__content'>
                  About Me
                </h2>
              </div>
              <p className='about__content'>Galli, quos praedae
                populationumque conciuerat spes, postquam pro eo ut ipsi ex
                alieno agro raperent agerentque, suas terras sedem belli esse
                premique utriusque partis exercituum hibernis uidere, uerterunt
                retro in Hannibalem ab Romanis odia; petitusque saepe principum
                insidiis, ipsorum inter se fraude, eadem leuitate qua
                consenserant consensum indicantium, seruatus erat et mutando
                nunc uestem nunc tegumenta capitis errore etiam sese ab insidiis
                munierat.
              </p>
              <div className='categories flex-v-center'>
                <p className={'categories__title'}>Categories:</p>
                <button className={'categories__button'}>cleaning</button>
                <button className={'categories__button'}>handy man</button>
                <button className={'categories__button'}>painting</button>
                <button className={'categories__button'}>delivery</button>
              </div>
            </section>
            <section className={'portfolio'}>
              <div className='blockTitle flex-v-center'>
                <img src={imageIcon} alt="" className=''/>
                <h2 className='blockTitle__content'>
                  Photo Portfolio
                </h2>
              </div>
              <div className={'portfolio__imageContainer'}>
                <img src={portfolio1} alt="" className='portfolio__image'/>
                <img src={portfolio3} alt="" className='portfolio__image'/>
                <img src={portfolio2} alt="" className='portfolio__image'/>
                <div className='portfolio__imageDummy flex-v-center'>
                  <img src={imageDummy} alt=""/>
                </div>
              </div>
            </section>
            <section className={'review'}>
              <div className='review__header'>
                <div className='blockTitle flex-v-center'>
                  <i className="material-icons blockTitle__icon">&#xE0CA;</i>
                  <h2 className='blockTitle__content'>
                    Alice Cooper's reviews
                  </h2>
                </div>
                <div className={'rating'}>
                  <p className={'rating__title'}>General rating:</p>
                  <div className={'stars'}>
                    <i className
                           ="fa fa-star fa-lg stars__item stars__item_filled">
                    </i>
                    <i className
                           ="fa fa-star fa-lg stars__item stars__item_filled">
                    </i>
                    <i className
                           ="fa fa-star fa-lg stars__item stars__item_filled">
                    </i>
                    <i className="fa fa-star fa-lg stars__item">
                    </i>
                    <i className="fa fa-star fa-lg stars__item">
                    </i>
                  </div>
                </div>
              </div>
              <div className='review__itemList'>
                <div className='review__item'>
                  <img src={avatarJm} alt="" className='review__item__avatar'/>
                  <div className='review__item__content'>
                    <h3 className='review__item__name'>
                      Jim Morrison <span className='review__item__time'>
                        <i className="fa fa-circle" aria-hidden="true">
                        </i> today
                      </span>
                    </h3>
                    <a href="#" className='review__item__link'>
                      Clean my house
                    </a>
                    <p className='review__item__text'>
                      Ceterum hic quoque ei timor causa fuit maturius mouendi
                      ex hibernis. Per idem tempus Cn. Seruilius consul Romae
                      idibus Martiis magistratum iniit. Ibi cum de re publica
                      rettulisset, redintegrata in C.
                    </p>
                  </div>
                  <div className='stars'>
                    <i className
                           ="fa fa-star fa-lg stars__item stars__item_filled">
                    </i>
                    <i className
                           ="fa fa-star fa-lg stars__item stars__item_filled">
                    </i>
                    <i className
                           ="fa fa-star fa-lg stars__item stars__item_filled">
                    </i>
                    <i className="fa fa-star fa-lg stars__item"></i>
                    <i className="fa fa-star fa-lg stars__item"></i>
                  </div>
                </div>
                <div className='review__item'>
                  <img src={avatarDb} alt="" className='review__item__avatar'/>
                  <div className='review__item__content'>
                    <h3 className='review__item__name'>
                      David Bowie <span className='review__item__time'>
                        <i className="fa fa-circle" aria-hidden="true">
                        </i> 6 hours ago
                      </span>
                    </h3>
                    <a href="#" className='review__item__link'>
                      Clean my house
                    </a>
                    <p className='review__item__text'>
                      Ceterum hic quoque ei timor causa fuit maturius mouendi
                      ex hibernis. Per idem tempus Cn. Seruilius consul Romae
                      idibus Martiis magistratum iniit. Ibi cum de re publica
                      rettulisset, redintegrata in C.
                    </p>
                  </div>
                  <div className={'stars'}>
                    <i className
                           ="fa fa-star fa-lg stars__item stars__item_filled">
                    </i>
                    <i className
                           ="fa fa-star fa-lg stars__item stars__item_filled">
                    </i>
                    <i className
                           ="fa fa-star fa-lg stars__item stars__item_filled">
                    </i>
                    <i className
                           ="fa fa-star fa-lg stars__item stars__item_filled">
                    </i>
                    <i className
                           ="fa fa-star fa-lg stars__item stars__item_filled">
                    </i>
                  </div>
                </div>
              </div>
              <div className='pagination'>
                <ul className='pagination__container'>
                  <li className='pagination__item'>
                    <a href="/">
                      <i className='fa fa-angle-double-left' aria-hidden="true">
                      </i>
                    </a>
                  </li>
                  <li className='pagination__item pagination__item_active
                    pagination__item_bordered'>
                    <a href="/">1</a>
                  </li>
                  <li className='pagination__item'>
                    <a href="/">2</a>
                  </li>
                  <li className='pagination__item'>
                    <a href="/">
                      3
                    </a>
                  </li>
                  <li className='pagination__item pagination__item_active'>
                    <a href="/">
                      <i className='fa fa-angle-double-right'
                         aria-hidden="true">
                      </i>
                    </a>
                  </li>
                </ul>
              </div>

            </section>
          </div>
        </div>
      </div>
  );
}
