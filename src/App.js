import React from 'react';
import Header from './components/header/Header';
import Content from './components/content/Content';
import Footer from './components/footer/Footer';

export default function App() {
  return (
      <div>
        <Header/>
        <Content/>
        <Footer/>
      </div>
  );
}
